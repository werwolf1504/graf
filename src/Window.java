import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

/** \mainpage Strona głowna
 *
 * \section intro_sec Wstęp
 * Tematem projektu jest realizownie programu do wyszukiwania grafów chemicznych połączeń za podanymi danymi. Program jest zrealizowany w języku programowania Java z wykożystaniem technologji „Java GUI”. Projram jest napisany w środowisku „IntelliJ IDEA”.
 */
/**
 * Klasa Window.
 * Klasa do twożenia interfejsu graficznego, zawiera wszystkie elementy taki jak: Przyciski, Pola Tekstowe.
 */
public class Window extends JFrame implements ActionListener{
    /**
     * Przycisk do wyjścia z programu.
     */
    JButton bExit;
    /**
     * Przycisk do wygenerowania grafu.
     */
    JButton bGenerate;
    /**
     * Przycisk do wyciśczenia.
     */
    JButton bDel;
    /**
     * Przycisk do otwużenia pliku z resultatem.
     */
    JButton bOpen;
    /**
     * Pole pokazujące że to jest element H.
     */
    JLabel lH;
    /**
     * Pole pokazujące że to jest element O.
     */
    JLabel lO;
    /**
     * Pole pokazujące że to jest element C.
     */
    JLabel lC;
    /**
     * Pole tekstowe do wpisywania elości elementu H.
     */
    JTextField tH;
    /**
     * Pole tekstowe do wpisywania elości elementu O.
     */
    JTextField tO;
    /**
     * Pole tekstowe do wpisywania elości elementu C.
     */
    JTextField tC;

    /**
     * Metoda do twożenia pola głównego.
     */
    public Window() {
        int Default_Wigth = 260;
        int Default_Heigth = 340;
        setSize(Default_Wigth,Default_Heigth);
        setTitle("Grafs");
        setLayout(null);

        lH = new JLabel("Element H");
        lH.setBounds(70,25,100,20);
        add(lH);

        tH = new JTextField("0");
        tH.setBounds(170,25,20,20);
        add(tH);

        lO = new JLabel("Element O");
        lO.setBounds(70,50,100,20);
        add(lO);

        tO = new JTextField("0");
        tO.setBounds(170,50,20,20);
        add(tO);

        lC = new JLabel("Element C");
        lC.setBounds(70,75,100,20);
        add(lC);

        tC = new JTextField("0");
        tC.setBounds(170,75,20,20);
        add(tC);

        bGenerate = new JButton("Generation");
        bGenerate.setBounds(80,110,100,35);
        bGenerate.addActionListener(this);
        add(bGenerate);

        bDel = new JButton("Delete");
        bDel.setBounds(80,155,100,35);
        bDel.addActionListener(this);
        add(bDel);

        bOpen = new JButton("Open");
        bOpen.setBounds(80,200,100,35);
        bOpen.addActionListener(this);
        add(bOpen);

        bExit = new JButton("Exit");
        bExit.setBounds(80,245,100,35);
        bExit.addActionListener(this);
        add(bExit);
        
    }

    /**
     * Metoda uruchomienia programu.
     * @param args
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Window frame = new Window();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }


    /**
     * Metoda przepisana.
     * Poslugiwana jest do śledzenia za tym, co jest używane, oraz wywołamia metod.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object ob1 = e.getSource();

        if (ob1 == bExit) {
            dispose();
        }
        if (ob1 == bDel){
            tH.setText("");
            tO.setText("");
            tC.setText("");
        }
        if (ob1 == bGenerate){
            Generate g = new Generate();
            int h = Integer.parseInt(tH.getText());
            int o = Integer.parseInt(tO.getText());
            int c = Integer.parseInt(tC.getText());
            try {
                g.Proverka(g.genetarion(h,o,c));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        if (ob1 == bOpen)
        {
            try {
                Desktop.getDesktop().edit(new File("Result.txt"));
            } catch (IOException e1) {

            }
        }
    }

}
