import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Klasa do generowania grafu
 */
public class Generate {
    /**
     * Zmienna zawierająca znaczenia walentności elcementów chemicznych.
     */
    int[] val = {1,2,4};


    /**
     * Funkcja do wwygenerowania riadu walentwości.
     * @param iloscH ilość elementu chemicznego H
     * @param iloscO ilość elementu chemicznego O
     * @param iloscC ilość elementu chemicznego C
     * @return Zwraca riad walentności
     */
    int[] genetarion(int iloscH, int iloscO, int iloscC){

        int[] valRad = new int[iloscH+iloscO+iloscC];
        for(int i = 0;i<iloscH;i++){
            valRad[i]=val[0];
        }
        for(int i = iloscH;i<iloscH+iloscO;i++){
            valRad[i]=val[1];
        }
        for(int i = iloscH+iloscO;i<iloscH+iloscO+iloscC;i++){
            valRad[i]=val[2];
        }

        return valRad;
    }

    /**
     * Metoda do sprawdzenia oraz tworzenia rądów grafu.
     * @param radVal pobiera rad walentnoości
     * @throws IOException
     */
    void Proverka(int[] radVal) throws IOException {
        int[] rad = new int[radVal.length];

        File file = new File("Result.txt");
        file.createNewFile();

        String lineSeoarator = System.getProperty("line.separator");

        FileWriter writer = new FileWriter(file);


        for (int i=0;i<rad.length;i++) {
            int sum = 0;
            writer.write("Rayad " + (i+1));
            //i--;
            writer.write(lineSeoarator);
            for (int j =0;j<rad.length;j++){
                try {
                    int k = j+1;
                    rad[k] = radVal[k-1];
                    for (int a = 0; a < rad.length; a++) {
                        sum = sum + rad[a];
                    }
                    //System.out.println("Suma: "+sum);
                    if (sum < radVal[i]) {
                        rad[k] = radVal[k-1];
                        k++;
                    }
                    if (sum == radVal[i]) {
                        for (int a = 0; a < rad.length; a++) {
                            writer.write(String.valueOf(rad[a]));
                            rad[a] = 0;
                        }
                        writer.write(lineSeoarator);
                    }
                    if (sum > radVal[i]) {
                        for (int a = 0; a < rad.length; a++) {
                            rad[a] = 0;
                        }
                        writer.write(lineSeoarator);
                    }
                    sum = 0;
                }catch (ArrayIndexOutOfBoundsException e){
                    continue;
                }
            }
        }
        writer.flush();
        writer.close();

        Desktop.getDesktop().edit(file);
    }
}
