import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Klasa testowa.
 *Klasa do testowania klasy "Genetate".
 */
public class GenerateTest {

    Generate g = new Generate();
    int[] radExt = {1,1,2};
    int[] radAct = g.genetarion(2,1,0);

    /**
     * Unit Test go metody "generation".
     */
    @Test
    public void genetarion() {
        Assert.assertArrayEquals(radExt,radAct);
    }
}